import Vuex from 'vuex'

const createStore = () => {
  return new Vuex.Store({
    state: () => ({
      counter: 0
    }),
    mutations: {
      increment (state) {
        state.counter++
      }
    },
    actions: {
        counterUp({ state, commit }){
          commit('setCounter', state.counter + 1)
        }
    },
    getters: {
        myGetter(state){ return state.counter + 1000}
    }
  })
}

export default createStore





// // holds your root state
// export const state = () => ({
//     counter: 0
//   })
  
//   // contains your actions
//   export const actions = {
//     counterUp({ state, commit }){
//       commit('setCounter', state.counter + 1)
//     }
//   }
//   // contains your mutations
//   export const mutations = {
//     setCounter(state, value){
//       state.counter = value
//     }
//   }
//   // your root getters
//   export const getters = {
//       myGetter(state){ return state.counter + 1000}
//   }